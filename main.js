class Objeto {
    constructor(objeto = {}){
        this.nombre = objeto.nombre || ''
        this.descripcion = objeto.descripcion || ''
        this.precio = objeto.precio || 0
    }
}

const app = Vue.createApp({
    data() {
        return {
            newTodo: '',
            editNombre: '',
            editDescripcion: '',
            editPrecio: '',
            error: false,
            nombre: '',
            descripcion: '',
            precio: '',
            objetos: [new Objeto({nombre: 'Unicornio', descripcion:'Dos cuernos', precio:1000})],
            editIndex: -1,
        }
    },
    methods:{
        addItem(){
            if(this.nombre.length>0 && this.descripcion.length>0 && this.precio>0){
             this.objetos.push({
                 nombre: this.nombre,
                 descripcion: this.descripcion,
                 precio:this.precio
            });
            }else{
                this.error =true;
            }
            
        },
        setItem(index){
            this.editIndex = index;
        },

        cancel(){
            this.editIndex = -1;
        },

        saveItem(index){
            this.objetos[index].nombre=this.editNombre;
            this.objetos[index].descripcion=this.editDescripcion;
            this.objetos[index].precio=this.editPrecio;
            this.editIndex=-1;
        },

        deleteItem(index){
            this.objetos.splice(index, 1);
        }
    }
})

app.mount('#app')